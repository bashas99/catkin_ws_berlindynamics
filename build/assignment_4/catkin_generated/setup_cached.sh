#!/usr/bin/env sh
# generated from catkin/python/catkin/environment_cache.py

# based on a snapshot of the environment before and after calling the setup script
# it emulates the modifications of the setup script without recurring computations

# new environment variables

# modified environment variables
export CMAKE_PREFIX_PATH="/home/b/catkin_ws_berlindynamics/devel/.private/assignment_4:$CMAKE_PREFIX_PATH"
export LD_LIBRARY_PATH="/home/b/catkin_ws_berlindynamics/devel/.private/assignment_4/lib:$LD_LIBRARY_PATH"
export PKG_CONFIG_PATH="/home/b/catkin_ws_berlindynamics/devel/.private/assignment_4/lib/pkgconfig:$PKG_CONFIG_PATH"
export PWD='/home/b/catkin_ws_berlindynamics/build/assignment_4'
export ROSLISP_PACKAGE_DIRECTORIES="/home/b/catkin_ws_berlindynamics/devel/.private/assignment_4/share/common-lisp:$ROSLISP_PACKAGE_DIRECTORIES"
export ROS_PACKAGE_PATH="/home/b/catkin_ws_berlindynamics/src/assignment_4:$ROS_PACKAGE_PATH"