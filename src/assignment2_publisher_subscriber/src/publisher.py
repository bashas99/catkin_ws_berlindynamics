#!/usr/bin/env python
import rospy
import std_msgs.msg
from autominy_msgs.msg import NormalizedSteeringCommand
from autominy_msgs.msg import SpeedCommand

def publisher():
	rospy.init_node("publisherSpeedSteering")
	steering_publisher = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=10)
	speed_publisher = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=10)

	while not rospy.is_shutdown():
		header = std_msgs.msg.Header()
		header.stamp = rospy.Time.now()
		steering_publisher.publish(header, 1.0)
		speed_publisher.publish(header, 0.3)

		rospy.sleep(0.5)
        
if __name__ == '__main__':
	try:
		publisher()
	except rospy.ROSInterruptException:
		pass

