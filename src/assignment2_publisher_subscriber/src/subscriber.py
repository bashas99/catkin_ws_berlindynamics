#!/usr/bin/env python3
import rospy
from autominy_msgs.msg import Speed

def callback(message):
    rospy.loginfo(message)

def subsriber():
    rospy.init_node("subscriberSpeed")
    rospy.Subscriber("/sensors/speed", Speed, callback)
    rospy.spin()

if __name__ == '__main__':
	try:
		subsriber()
	except rospy.ROSInterruptException:
		pass

