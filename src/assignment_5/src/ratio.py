#!/usr/bin/env python3
import rospy
from autominy_msgs.msg import Tick
from autominy_msgs.msg import NormalizedSteeringCommand
from autominy_msgs.msg import SpeedCommand


def callback(message):
    print(message)
    

def subsriber():
    rospy.Subscriber("/sensors/arduino/ticks", Tick, callback)
    rospy.spin()

if __name__ == '__main__':
	try:
	        rospy.init_node("ratioSpeedTick")
	        subsriber()
	except rospy.ROSInterruptException:
		pass

