#!/usr/bin/env python

import rospy
import numpy as np
import cv2 as cv
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from math import sqrt
from std_msgs.msg import Float64MultiArray


class Linear:
	
	def __init__(self, a, b):
		self.a, self.b = a, b
		
	@classmethod
	def fit(cls, x1, y1, x2, y2):
		a = (y1 - y2) / (x1 - x2)
		b = y1 - a*x1
		return cls(a, b)
		
	def distance(self, x, y):
		return np.abs(self.a*x - y + self.b) / np.sqrt(self.a*self.a + 1)
	
	def __call__(self, x):
		return self.a*x + self.b
	
	def draw(self, image, color, thickness):
		m, n = image.shape
		sp, ep = (int(self(0)), 0), (int(self(m-1)), m-1)
		return cv.line(image, sp, ep, color, thickness)
		
		
def estimate_linear(points2d: np.ndarray, k: int, d: int, t: float, verbose=False):
    best_linear = None
    best_error = float('inf')
    xs, ys = points2d.T
    for _ in range(k):
        (x1, y1), (x2, y2) = points2d[np.random.choice(len(points2d), size=2, replace=False)]  # maybe inliers
        linear = Linear.fit(x1, y1, x2, y2)
        
        distance = linear.distance(xs, ys)
        idx, = np.where(distance < t)
        
        if len(idx) > d:
            this_error = np.mean(distance[idx])
            if this_error < best_error:
                best_error, best_linear = this_error, linear
    return best_linear


rospy.init_node('estimator')
estimator_publisher = rospy.Publisher('/estimation', Image, queue_size=10)
parameter_publisher = rospy.Publisher('/parameter', Float64MultiArray, queue_size=10)


def callback(message):
	k, d, t = 1_000, 750, 20.0

	bridge = CvBridge()
	image = bridge.imgmsg_to_cv2(message, desired_encoding='passthrough')
	
	
	
	image_mask = image.copy()
	image_show = image.copy()
	image_mask[:150,:] = 0
	image_show[:150,:] = 0
	
	points2d = np.stack(np.where(image_mask == 255)).T
	linear = estimate_linear(points2d, k, d, t)
	
	image_show = linear.draw(image_show, 127, 2)
	image_mask = linear.draw(image, 0, int(2*t))
	
	points2d = np.stack(np.where(image_mask == 255)).T
	linear_ = estimate_linear(points2d, k, d, t)
	image_show = linear_.draw(image_show, 127, 2)
		
	message = bridge.cv2_to_imgmsg(image_show, encoding='passthrough')
	estimator_publisher.publish(message)
	parameter_publisher.publish(Float64MultiArray(data=[linear.a, linear.b, linear_.a, linear_.b]))
	
	
subscriber = rospy.Subscriber('/threshold', Image, callback)
rospy.spin()
