#!/usr/bin/env python

import rospy
import numpy as np
import cv2 as cv
from sensor_msgs.msg import Image
from cv_bridge import CvBridge


rospy.init_node('thresholder')
publisher = rospy.Publisher("/threshold", Image, queue_size=10)


def callback(message):
	bridge = CvBridge()
	image = bridge.imgmsg_to_cv2(message, desired_encoding='passthrough')
	print(image.shape)
	_, thresh = cv.threshold(image,200,255,cv.THRESH_BINARY)
	message = bridge.cv2_to_imgmsg(thresh, encoding='passthrough')
	publisher.publish(message)
	
	
subscriber = rospy.Subscriber('/sensors/camera/infra1/image_rect_raw', Image, callback)
rospy.spin()
