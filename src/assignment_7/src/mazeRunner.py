#!/usr/bin/env python
import rospy
import std_msgs.msg
import numpy as np
from autominy_msgs.msg import SpeedCommand, NormalizedSteeringCommand
from sensor_msgs.msg import LaserScan

rospy.init_node("mazeRunner")
steering_publisher = rospy.Publisher("/actuators/steering_normalized", NormalizedSteeringCommand, queue_size=1)
speed_publisher = rospy.Publisher("/actuators/speed", SpeedCommand, queue_size=1)

def movement(speed, steering): 
	print("in movement")
	header = std_msgs.msg.Header()
	header.stamp = rospy.Time.now()
	steering_publisher.publish(header, steering)
	speed_publisher.publish(header, speed)

def callback(msg):
	ranges = np.array(msg.ranges, dtype=np.float_)
	angles = np.linspace(start=msg.angle_min, stop=msg.angle_max, num=len(ranges))
	
	if (ranges[0] < 1.5) or (ranges[90] < 0.25) or (ranges[240] < 0.25):
		print(ranges[0])    #Distance to the nearest front obstacle.
		print(ranges[90])   #Distance to the nearest obtacle 45 degrees to the right.
		print(angles[90])
		print(ranges[270])  #Distance to the nearest obstacle 45 degrees to the left.
		print(angles[270])
		print("-------------------------------------------")
		if (ranges[90]) > (ranges[270]):
			movement(0.3,0.9)
		else:
			movement(0.3,-0.9)
	else:
		movement(0.3,0)   #No obstacle
        
if __name__ == '__main__':
	try:
		rospy.Subscriber('/sensors/rplidar/scan', LaserScan, callback)
		rospy.spin()
	except rospy.ROSInterruptException:
		pass

