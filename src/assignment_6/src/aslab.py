#!/usr/bin/env python

import rospy
import numpy as np

import std_msgs.msg

from sensor_msgs.msg import PointCloud2
import tf2_ros
from tf.transformations import quaternion_matrix
from sensor_msgs.point_cloud2 import create_cloud_xyz32, read_points


rospy.init_node('aslabnode', anonymous=True)


tf_buffer = tf2_ros.Buffer()
listener = tf2_ros.TransformListener(tf_buffer)

publisher = rospy.Publisher('/aslab', PointCloud2, queue_size=10)




def callback(msg):
	points = np.array(list(read_points(msg)), dtype=np.float_)
	points = np.hstack( (points, np.ones((len(points), 1))) )
	try:
		trans = tf_buffer.lookup_transform('lab', 'laser', rospy.Time.now(), rospy.Duration(0.001))
	except Exception as e:
		print(e)
	else:
		rotation = trans.transform.rotation
		translation = trans.transform.translation
		mat = quaternion_matrix([rotation.x, rotation.y, rotation.z, rotation.w])
		mat[0:3, 3] = [translation.x, translation.y, translation.z]
		
		header = std_msgs.msg.Header()
		header.stamp = rospy.Time.now()
		header.frame_id = 'lab'
		
		rpoints = (points @ mat.T)[:, :3]# rotate and translate
		
		cloud = create_cloud_xyz32(header, rpoints)
		publisher.publish(cloud)
		print("published")


rospy.Subscriber('/cartesianlaser', PointCloud2, callback)
rospy.spin()



