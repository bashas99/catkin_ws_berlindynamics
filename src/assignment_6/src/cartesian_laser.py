#!/usr/bin/env python

import rospy
import numpy as np

import std_msgs.msg
from sensor_msgs.point_cloud2 import create_cloud_xyz32, read_points
from sensor_msgs.msg import LaserScan, PointCloud2


rospy.init_node('cartesianlasernode', anonymous=True)


def rotation_z(angles, ranges):
	
	result = np.empty((len(ranges), 3))
	result[:, 0] = ranges * np.cos(angles)
	result[:, 1] = ranges * np.sin(angles)
	result[:, 2] = 0
	return result
	
	
	
publisher = rospy.Publisher('/cartesianlaser', PointCloud2, queue_size=10)



def callback(msg):
	ranges = np.array(msg.ranges, dtype=np.float_)
	angles = np.linspace(start=msg.angle_min, stop=msg.angle_max, num=len(ranges))
	
	idx = np.isfinite(ranges)
	ranges = ranges[idx]
	angles = angles[idx]
	
	points = rotation_z(angles, ranges)
	
	
	header = std_msgs.msg.Header()
	header.stamp = rospy.Time.now()
	header.frame_id = 'laser'
	
	cloud = create_cloud_xyz32(header, points)
	publisher.publish(cloud)
	print("published")



rospy.Subscriber('/sensors/rplidar/scan', LaserScan, callback)
rospy.spin()
