#!/usr/bin/env python

import rospy
import numpy as np

import std_msgs.msg
from geometry_msgs.msg import Point
from nav_msgs.msg import OccupancyGrid, MapMetaData, Odometry
from sensor_msgs.point_cloud2 import read_points
from sensor_msgs.msg import PointCloud2

HEIGHT, WIDTH = 900, 800


rospy.init_node('gridnode', anonymous=True)


publisher = rospy.Publisher('/occupancygrid', OccupancyGrid, queue_size=10)


def swapxy(arr):
	swapped = np.empty_like(arr)
	swapped[:,0], swapped[:,1] = arr[:,1], arr[:,0]
	return swapped



def callback(msg):
	points = np.array(list(read_points(msg)), dtype=np.float_)
	data = np.zeros(shape=(HEIGHT, WIDTH), dtype=np.int_)
	points = swapxy(points) # This helps to align the grid somehow
	points = (100 * points[:, :2]).astype(np.int_)
	points = points[np.all(points >= 0, axis=1) & (points[:, 0] < WIDTH) & (points[:, 1] < HEIGHT)]
	data[points[:,0], points[:,1]] = 100
	print(data)
	print(points) 
	
	info = MapMetaData(resolution=0.01, width=WIDTH, height=HEIGHT)
	
	header = std_msgs.msg.Header()
	header.stamp = rospy.Time.now()
	header.frame_id = 'lab'
	
	grid = OccupancyGrid(header=header, info=info, data=data.reshape(-1).tolist())
	
	publisher.publish(grid)
	

rospy.Subscriber('/aslab', PointCloud2, callback)
rospy.spin()
