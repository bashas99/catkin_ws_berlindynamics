#!/usr/bin/env python
import rospy
import numpy as np
import cv2 as cv
import std_msgs.msg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

def callback(message):
    #Bridge between a ROS image message and an OpenCV image
    bridge = CvBridge()
    cv_image = bridge.imgmsg_to_cv2(message, desired_encoding='passthrough')
    #We chose 200 as the threshold for each pixel
    ret,thresh = cv.threshold(cv_image,200,255,cv.THRESH_BINARY)
    thresh = thresh.copy()
    #Drawing black rectangles to remove remaining stray white points
    thresh[0:thresh.shape[0]//4,:(thresh.shape[1]//2)]=0
    thresh[0:thresh.shape[0]//5,(thresh.shape[1]//2):]=0
    thresh[:,:(thresh.shape[1]//3 - 20)]=0
    thresh[(thresh.shape[0]//2):,:]=0
    image_message = bridge.cv2_to_imgmsg(thresh, encoding="passthrough")
    img_publisher.publish(image_message)

def subsriber():
    rospy.Subscriber("/sensors/camera/infra1/image_rect_raw", Image, callback)
    rospy.spin()

if __name__ == '__main__':
	try:
		rospy.init_node("publisherThreshedImage")
		img_publisher = rospy.Publisher("/Threshold",Image, queue_size=10)
		subsriber()
	except rospy.ROSInterruptException:
		pass        
        

