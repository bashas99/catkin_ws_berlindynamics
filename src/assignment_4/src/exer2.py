#!/usr/bin/env python
import rospy
import numpy as np
import cv2 as cv
import std_msgs.msg
from sensor_msgs.msg import CameraInfo
from cv_bridge import CvBridge
from numpy.linalg import inv

def callback(message):
    d = message.D
    print(message.D)
    k = message.K
    print(message.K)

def subsriber():
    rospy.Subscriber("/sensors/camera/infra1/camera_info", CameraInfo, callback)
    rospy.spin()

if __name__ == '__main__':
	try:
		rospy.init_node("parameterReader")
		subsriber()
	except rospy.ROSInterruptException:
		pass        
        

