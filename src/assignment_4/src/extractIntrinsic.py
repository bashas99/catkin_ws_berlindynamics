#!/usr/bin/env python
import rospy
import numpy as np
import cv2 as cv
import std_msgs.msg
from sensor_msgs.msg import CameraInfo
from cv_bridge import CvBridge
from numpy.linalg import inv

def callback(message):
    k = message.K
    print("The intrinsic parameters fx, fy, cx, cy are: ")
    print(k[0],k[4],k[2],k[5])
    
    d = message.D
    print("The distortion coefficients are: ")
    print(message.D)

def subsriber():
    rospy.Subscriber("/sensors/arduino/ticks", CameraInfo, callback)
    rospy.spin()

if __name__ == '__main__':
	try:
		rospy.init_node("parameterReader")
		subsriber()
	except rospy.ROSInterruptException:
		pass        
        

