#!/usr/bin/env python
import rospy
import numpy as np
import cv2 as cv
import std_msgs.msg
from sensor_msgs.msg import CameraInfo
from cv_bridge import CvBridge
from numpy.linalg import inv

def callback(message):
    d = message.D
    k = message.K
    cameraMatrix = np.array([[k[0],k[1],k[2]], [k[3],k[4],k[5]], [k[6],k[7],k[8]]])
    distCoeffs = np.array(d)
    
    worldPoints = np.array([[1.1,-0.2,0],[1.1,0.2,0],[0.8,-0.2,0],[0.8,0.2,0],[0.5,-0.2,0],[0.5,0.2,0]])
    #Camera points copied from findWhite.py result. Since these points are only used once, there is no point in setting up findWhite as a publisher or service.
    cameraPoints = np.array([[113.0, 417.0], [122.04347826086956, 267.6521739130435], [150.4, 445.5], [162.48888888888888, 246.37777777777777], [224.22360248447205, 504.58385093167703], [236.5326086956522, 207.65217391304347]])
    retval, rvec, tvec = cv.solvePnP(worldPoints, cameraPoints, cameraMatrix, distCoeffs)
    rotMatrix, jacobian = cv.Rodrigues(rvec)
    
    print("The rotation vector is: ")
    print(rvec)
    
    print("The transaltion vector is: ")
    print(tvec)
    
    
    print("The rotation matrix is: ")
    print(rotMatrix)
    
    print("The 4x4 homogeneous matrix is: ")
    homogeneousMat = np.vstack([np.concatenate((rotMatrix, tvec), axis=1),(np.array([0,0,0,1]))])
    print(homogeneousMat)
    
    print("The inverse of the homogeneous matrix is: ")
    print(inv(homogeneousMat))


def subsriber():
    rospy.Subscriber("/sensors/camera/infra1/camera_info", CameraInfo, callback)
    rospy.spin()

if __name__ == '__main__':
	try:
		rospy.init_node("parameterReader")
		subsriber()
	except rospy.ROSInterruptException:
		pass        
        

