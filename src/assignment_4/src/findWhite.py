#!/usr/bin/env python
import rospy
import numpy as np
import cv2
import std_msgs.msg
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
from sklearn.cluster import KMeans


def clusterfuck(X): 
   clusters, lables, rows, cols, clusterNumber = {}, {}, X.shape[0], X.shape[1], 1
   for i in range(0,rows):      #Initialize labels matrix
      for j in range(0,cols):
         lables[(i,j)] = 0
         
   for i in range(1,rows-1):    #Assign each white point a cluster
      for j in range(1,cols-1):
         if (X[i][j] == 0): #If point is black, then ignore.
            continue
         #Neighbors contains the label of each point surrounding the current one.
         neighbours = [lables[(i,j-1)],lables[(i-1,j-1)],lables[(i-1,j)],lables[(i-1,j+1)],lables[(i,j+1)],lables[(i+1,j+1)],lables[(i+1,j)],lables[(i+1,j-1)]]
         neighboursC = [X[i,j-1],X[i-1,j-1],X[i-1,j],X[i-1,j+1],X[i,j+1],X[i+1,j+1],X[i+1,j],X[i+1,j-1]]
         #If white point isn't surrounded by black points only and none of its neighbors are labled, give point a new label.
         if (X[i][j] == 255 and (allEqualZero(neighbours)) and (not allEqualZero(neighboursC))):
            lables[(i,j)] = clusterNumber
            clusterNumber +=1
            continue
         #Otherwise assign the point to the cluster of its neighbours.
         else:
            for n in neighbours:
               if n!=0:
                  lables[(i,j)] = n
                  break            
   #The rest of the function cleans and orders the clusters, then calculates the center of each cluster and returns a dictionary.
   for p in lables:
      if X[p[0],p[1]] == 0:
         continue
      else:
         if lables[p] not in clusters:
            clusters[lables[p]]=[p]
         else:
            clusters[lables[p]].append(p)
   centers = {}
   trunc = 0
   for c in sorted(clusters):
      centX, centY = 0, 0
      if len(clusters[c]) < 10:
         trunc += 1
         continue
      for x,y in clusters[c]:
         centX+=x
         centY+=y
      centers[c-trunc] = (centX/len(clusters[c]), centY/len(clusters[c]))
   return centers

def allEqualZero(arr):
   for i in arr:
      if i != 0:
         return False
   return True


def callback(message):
    bridge = CvBridge()
    X = bridge.imgmsg_to_cv2(message, desired_encoding='passthrough')
    clusters = clusterfuck(X)
    print(clusters)
    
def subsriber():
    rospy.Subscriber("/Threshold", Image, callback)
    rospy.spin()

if __name__ == '__main__':
	try:
		rospy.init_node("clusteringNode")
		subsriber()
	except rospy.ROSInterruptException:
		pass  

