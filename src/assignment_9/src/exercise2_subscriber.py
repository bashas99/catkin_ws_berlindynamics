#!/usr/bin/env python

import rospy
import numpy as np
from math import sqrt
from autominy_msgs.msg import SpeedCommand
import std_msgs.msg
from nav_msgs.msg import Odometry


rospy.init_node('speedsubscriber')


class Subscriber:
	def __init__(self):
		self.vector = None
		self.time = 0.
		self. subscriber = rospy.Subscriber('/simulation/odom_ground_truth', Odometry, self.callback)
		

	def callback(self, message):
		time = self.__stamp_to_time(message.header.stamp)
		vector = self.__position_to_vector(message.pose.pose.position)
		
		v = self.velocity(time, vector)
		print(f'{v:.10f}', time)
		self.time = time
		self.vector = vector
		
	@staticmethod
	def __stamp_to_time(stamp):
		return stamp.secs + stamp.nsecs*1e-9
	
	@staticmethod
	def __position_to_vector(position):
		return np.array((position.x, position.y, position.z))
	
	def velocity(self, time, vector):
		if self.time==0.0 or self.vector is None:
			v = 0.0
		else:
			dp = sqrt(np.sum(np.square(vector - self.vector)))
			dt = time - self.time
			v = dp / dt
		return v
			
		
subscriber = Subscriber()
rospy.spin()
