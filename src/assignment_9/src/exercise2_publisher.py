#!/usr/bin/env python

import rospy
import numpy as np
from math import sqrt
from autominy_msgs.msg import SpeedCommand
import std_msgs.msg
from nav_msgs.msg import Odometry


rospy.init_node('speedpublisher')
		


class Publisher:
	def __init__(self):
		self.publisher = rospy.Publisher('/actuators/speed', SpeedCommand, queue_size=1)
	
	def publish(self, stamp):
		self.publisher.publish(std_msgs.msg.Header(stamp=rospy.Time.now()), 100.0)	
	

class Subscriber:
	def __init__(self, publisher):
		self.publisher = publisher
		self.subscriber = rospy.Subscriber('/simulation/odom_ground_truth', Odometry, self.callback)
	
	@staticmethod
	def __stamp_to_time(stamp):
		return stamp.secs + stamp.nsecs*1e-9
	
	def callback(self, message):
		stamp = message.header.stamp
		time = self.__stamp_to_time(stamp)
		publisher.publish(stamp=stamp)
		print(time)
	

publisher = Publisher()
subscriber = Subscriber(publisher)

rospy.spin()
